/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arduino;

/**
 *
 * @author Oliver
 */
public class Testr {

	private static final int[] RGB_IN = {0xC0, 0xD1, 0xCE, 0xEB, 0xDE, 0xDC};
	private static final int[] RGB_OUT = new int[6];

	public static void main(String... args) {
		/**
		 *	For every number, we add it to the end of the data value then shift it across by 8 bits
		 *	Decoding is doing the same process in the reverse direction
		 * 
		 * Sending one number over the serial connection makes it much less likely to miss part of a value,
		 * it also means more data can be sent using less bandwidth.
		 * 
		 *	So having the RGB colours 0x45, 0xAA, 0x5C would be processed as the following:
		 *	Encode:
		 *	0x000000 + 0x45 = 0x000045
		 *	0x000045 << 8 = 0x004500
		 *	0x004500 + 0xAA = 0x0045AA
		 *	0x0045AA << 8 = 0x45AA00
		 *	0x45AA00 + 0x5C = 0x45AA5C
		 * 
		 *	Decode:
		 *	0x45AA5C & 0xFF = 0x5C
		 *	0x45AA5C >> 8 = 0x0045AA
		 *	0x0045AA & 0xFF = 0xAA
		 *	0x0045AA >> 8 = 0x000045
		 *	0x000045 & 0xFF = 0x45
		 *
		 */
		long data = 0x000000000000;

		for (int i = 0; i < RGB_IN.length - 1; i++) {
			data += RGB_IN[i];
			data = data << 8;
		}
		data += RGB_IN[RGB_IN.length - 1];

	
		print(data);
		
		
		for (int i = RGB_OUT.length - 1; i >= 0; i--) {
			RGB_OUT[i] = (int) (data & 0xFF);
			data = data >> 8;
		}

		for (int i = 0; i < RGB_OUT.length; i++) {
			print(RGB_OUT[i]);
		}

	}


	private static void print(long l) {
		System.out.println(String.format("0x%06X", l) + "\t\t" + l);
	}
}
