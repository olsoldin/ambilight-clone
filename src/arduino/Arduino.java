package arduino;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;
import processing.serial.Serial;
import processing.core.PApplet;

public class Arduino extends Thread {

	public static void main(String[] args) throws InterruptedException {

		for (String s : Serial.list()) {
			System.out.println(s);
		}

//		int lastVal = 0;
//		while(true){
//			PORT.write((byte)lastVal);
//            lastVal ^= 1;
//            System.out.println(lastVal);
//			Thread.sleep(250);
//		}
		Arduino ard = new Arduino();

		ard.start();

	}
	//--//
	private final static Serial PORT = new Serial(new PApplet(), Serial.list()[0], 9600);
	private Robot robby;
	//--//
	private final int SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	private final int SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	//--//
	private final int SKIP_PIXEL = 10;
	private final int CHECKED_WIDTH = SCREEN_WIDTH / SKIP_PIXEL;
	private int topPixel;
	private int bottomPixel;
	private final int[] RGB = new int[6];
	private final int[][] RGB_RAW = new int[6][CHECKED_WIDTH];
	//--//
	private final int CHECKED_HEIGHT = (SCREEN_HEIGHT / 4) / SKIP_PIXEL;
	private final int CHECKED_BOTTOM_PIXELS_START = CHECKED_HEIGHT * 3;

	private final Rectangle TOP_SCREEN = new Rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT / 4);
	private final Rectangle BOTTOM_SCREEN = new Rectangle(0, CHECKED_BOTTOM_PIXELS_START, SCREEN_WIDTH, SCREEN_HEIGHT / 4);

	private BufferedImage topScreenshot;
	private BufferedImage bottomScreenshot;

	public Arduino() {
		setup();
	}

	@Override
	public void run() {
		while (true) {
			draw();
			try {
				Thread.sleep(40);
			} catch (InterruptedException ex) {
				Logger.getLogger(Arduino.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	private void setup() {
		try {
			robby = new Robot();
		} catch (AWTException e) {
			System.out.println("Robot class not supported by your system!");
			System.exit(0);
		}
	}

	private void draw() {
		topScreenshot = robby.createScreenCapture(TOP_SCREEN);
		bottomScreenshot = robby.createScreenCapture(BOTTOM_SCREEN);

//		long startNano = System.nanoTime();
		calculateAverageRGB();
		sendColourOverSerial();
//		long endNano = System.nanoTime();
//		System.out.printf("Time taken: %fms%n", (endNano - startNano) / 1000000f);

	}

	private void calculateAverageRGB() {
		for (int x = 0; x < CHECKED_WIDTH; x++) {
			int px = x * SKIP_PIXEL;
			for (int y = 0; y < CHECKED_HEIGHT; y++) {
				int py = y * SKIP_PIXEL;
				topPixel = topScreenshot.getRGB(px, py); //the ARGB integer has the colors of pixel (i,j)
				bottomPixel = bottomScreenshot.getRGB(px, py);
				RGB_RAW[0][x] = 255 & (topPixel >> 16);
				RGB_RAW[1][x] = 255 & (topPixel >> 8);
				RGB_RAW[2][x] = 255 & (topPixel);
				RGB_RAW[3][x] = 255 & (bottomPixel >> 16);
				RGB_RAW[4][x] = 255 & (bottomPixel >> 8);
				RGB_RAW[5][x] = 255 & (bottomPixel);
			}
		}

		// Calculate average RGB values
		for (int i = 0; i < RGB_RAW.length; i++) {
			for (int j = 0; j < RGB_RAW[i].length; j++) {
				RGB[i] += RGB_RAW[i][j];
			}
			RGB[i] /= RGB_RAW[i].length;
		}

		//always too red, reduce the value by half
//		rgb[0] /= 2;
//		rgb[3] /= 2;
	}

	/**
	 * Data format:
	 * RGB_TOP_R
	 * RGB_TOP_G
	 * RGB_TOP_B
	 * RGB_BOTTOM_R
	 * RGB_BOTTOM_G
	 * RGB_BOTTOM_B
	 */
	private void sendColourOverSerial() {
		try {
			long data = 0x000000000000;
			PORT.write(0xff); //write marker (0xff) for synchronization
			for (int i = 0; i < RGB.length; i++) {
				data += RGB[i];
				data = data << 0;
				System.out.print(RGB[i]);
				System.out.print(" ");
				PORT.write(RGB[i]);
			}
			System.out.println("");
		} catch (NullPointerException e) {
			System.err.println(e);
		}
	}
}
